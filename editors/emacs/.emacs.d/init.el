;; Custom Functions
(defconst dmbash/init-file-location (expand-file-name "init.el" user-emacs-directory))
(defconst dmbash/user-file-location (concat user-emacs-directory "user"))
(defconst dmbash/packages-directory (concat dmbash/user-file-location "/packages"))

(defun dmbash/open-init-file ()
  "Open init.el."
  (interactive)
  (find-file dmbash/init-file-location))

(defun dmbash/open-conf-dir ()
  "Open my emacs config dir in dired-mode."
  (interactive)
  (dired user-emacs-directory))

(defun dmbash/reload-config ()
  (interactive)
  (load-file dmbash/init-file-location))

(defun dmbash/load-pkgs (pkgs-list)
  (dolist (pkg pkgs-list)
    (load-file
     (expand-file-name
      (concat pkg ".el")
      dmbash/packages-directory
      ))))

(defun dmbash/load-user-file (user-file)
  (load-file
   (expand-file-name user-file dmbash/user-file-location))
  )

;; Startup code
(setq gc-cons-threshold (* 50 1000 1000))
(setq read-process-output-max (* 1024 1024)) ;; 1mb
(setq comp-async-report-warnings-error nil)
(set-default-coding-systems 'utf-8)

(setq create-lockfiles nil)

;; straight.el bootstrap start
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))
;; straight.el bootstrap end

;; enable use-package with straight.el
(straight-use-package 'use-package)

(use-package no-littering
  :straight t
  :config
  (setq auto-save-file-name-transforms
      `((".*" ,(no-littering-expand-var-file-name "auto-save/") t)))
  )
;; custom file
(dmbash/load-user-file "custom.el")
;; load sane defaults
(dmbash/load-user-file "defaults.el")
;; load gui file
(dmbash/load-user-file "gui.el")

(dmbash/load-pkgs '("which-key"
		      "consult"
		      "vertico" ;; vertico + orderless + marginalia
		      "magit"
		      "diff-hl"
		      "projectile"
		      "hydra"
		      "dired"
		      "editorconfig"
		      "hl-todo"
		      "org"
		      "company"
		      "lsp-mode"
		      "smartparens"
		      "yasnippet"
		      "golang"
		      "web-mode"
		      "typescript"
		      ))

(setq gc-cons-threshold (* 50 1000 1000))
;; Startup ends here

;FIXME: hack, that muting error output of some timer
(setq show-paren-context-when-offscreen nil)
