;; Org package config
(use-package org
  :straight t
  :hook (org-mode-hook)
  :config
  (setq org-log-done 'time)
  (setq dashboard-set-heading-icon t)
  (setq dashboard-set-file-icons t)
  )
