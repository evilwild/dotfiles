(use-package web-mode
  :straight t
  :init
    (add-to-list 'auto-mode-alist '("\\.vue\\'" . web-mode))
  )
