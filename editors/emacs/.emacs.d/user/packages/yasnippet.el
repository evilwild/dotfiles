(use-package yasnippet
  :straight t
  :hook (prog-mode . yas-minor-mode)
  :diminish yas-minor-mode
  :custom
  (yas-prompt-function '(yas-completing-prompt))
  (yas-verbosity 1))

(use-package yasnippet-snippets
  :straight t
  :after yasnippet)
