;; https://github.com/michaeltd/.emacs.d/blob/master/readme.org					
(fset 'yes-or-no-p 'y-or-n-p) ; use y, n instead of yes or no

(global-set-key (kbd "C->") 'indent-rigidly-right-to-tab-stop)
(global-set-key (kbd "C-<") 'indent-rigidly-left-to-tab-stop)
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)
