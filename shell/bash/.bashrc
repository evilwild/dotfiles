shopt -s autocd

if [ -e $HOME/.bash_aliases ]; then
    source $HOME/.bash_aliases
fi

[[ $PS1 && -f /usr/share/bash-completion/bash_completion ]] && \
    . /usr/share/bash-completion/bash_completion

export PATH=$PATH:$HOME/dotfiles/scripts:$HOME/.local/bin:$GOPATH/bin
