\alias genkernel='genkernel --kernel-cc=/usr/lib/ccache/bin/gcc $@'
alias ll='ls -la'
alias emacs='emacs -nw'
alias lemacs="emacs -Q -l $HOME/.emacs.d/light-init.el"