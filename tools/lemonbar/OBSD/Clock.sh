#!/bin/sh

Clock() {
    DATETIME=$(date "+%a %b %d, %T")
    echo -n "%{F-}%{B-} $DATETIME %{F-}%{B-}"
}
