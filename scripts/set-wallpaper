#!/usr/bin/env bash
# Shell script for manipulating .wallpaper file & set bg with feh
# Dmitry Bashkirov @ 2021

# See https://stackoverflow.com/a/246128/3561275
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
    DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
    SOURCE="$(readlink "$SOURCE")"
    [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

source $DIR/lib/confirm

WALLPAPER_FILE="$HOME/.wallpaper"

usage="USAGE:
  $(basename $0) [file] [-r]
where:
  - file - image file to use as wallpaper
  - -r - refresh current wallpaper"

is_command_available () {
    if [ -x "$(command -v $1)" ]; then
	return 0
    else
	echo "No $1 available"
	return 1
    fi
}

is_converter_available () {
    # imagemagick installed ?
    is_command_available convert
    if [ "$?" -eq 0 ]; then
	return 0;
    else
	echo "Cannot convert to .png, either install ImageMagick or use .png files"
	return 1;
    fi
}

convert_image () {
    is_converter_available
    if [ "$?" -eq 0 ]; then
	dest_file_name="/tmp/$(basename "${1%.*}").png"
	convert "$1" "$dest_file_name"
	cp $dest_file_name $WALLPAPER_FILE
	rm $dest_file_name
	return 0;
    else
	return 1;
    fi
}

function help_message {
    echo "$usage"
    exit 1;
}

function set_wallpaper {
    echo "Setting .wallpaper"
    feh --bg-fill $WALLPAPER_FILE
    [ "$?" -eq 0 ] && echo "Opetation completed successfuly" || echo "Operation failed"
}

while getopts "hr" option; do
    case "$option" in
	r) REFRESH=1
	   ;;
	h) help_message
	   ;;
    esac
done
# remove OPTARG's from positional arguments to get file
shift $((OPTIND -1))

if [[ $REFRESH -eq 1 ]]; then
    is_command_available feh
    if [ "$?" -eq 0 ]; then
	set_wallpaper
    fi
    exit 1;
fi;

if file "$1" |grep -qE "image|bitmap"; then
    # https://gist.github.com/pirxpilot/4671190
    if test -e "$1" -a $(file -b --mime-type "$1") != "image/png"; then
	confirm "Image is not in PNG format, would you like to convert it?"
	if [ "$?" -eq 0 ]; then
	    convert_image "$1"
	    if [ "$?" -eq 0 ]; then
		set_wallpaper
	    fi
	fi
	exit 1;
    fi
    
    echo "Copying new wallpaper file..."
    cp "$1" $WALLPAPER_FILE
    set_wallpaper
else
    echo "Image file not given"
    exit 1;
fi
